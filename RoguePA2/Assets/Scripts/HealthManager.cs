﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

    public int  maxPlayerHealth;
    public static int playerHealth;

    private LevelManager levelManager;

    public bool isDead;

    Text text;

    void Start ()
    {
        text = GetComponent<Text>();

        playerHealth = maxPlayerHealth;

        levelManager = FindObjectOfType<LevelManager>();

        isDead = false; 
    }
	
	
	void Update ()
    {
	if(playerHealth <= 0 && !isDead)
        {
            playerHealth = 0;
            levelManager.respawnPlayer();
            isDead = true;
        }

        text.text = "" + playerHealth;
	}

    public static void HurtPlayer(int damageToGive)
    {
        playerHealth -= damageToGive;
    }

    public void FullHealth()
    {
        playerHealth = maxPlayerHealth;
    }

}
