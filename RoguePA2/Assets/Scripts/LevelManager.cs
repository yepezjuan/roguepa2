﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {


    public GameObject currentCheckpoint;
    public GameObject deathParticle;
    public GameObject respawnParticle;

    public int pointPenaltyOnDeath;

    public float respawnDelay;

    private CameraController Camera;
    private PlayerController player;
    private float gravityStore;

    public HealthManager healthmanager;

	void Start () {
        Camera = FindObjectOfType<CameraController>();
        player = FindObjectOfType<PlayerController>();
        healthmanager = FindObjectOfType<HealthManager>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void respawnPlayer()
    {
        StartCoroutine("RespawnPlayerCo");
    }

    public IEnumerator RespawnPlayerCo()
    {
        Instantiate(deathParticle, player.transform.position, player.transform.rotation);
        player.enabled = false;
        player.GetComponent<Renderer>().enabled = false;
        Camera.isFollowing = false;
        //gravityStore = player.GetComponent<Rigidbody2D>().gravityScale;
        //player.GetComponent<Rigidbody2D>().gravityScale = 0f;
        //player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        ScoreManager.AddPoints(-pointPenaltyOnDeath);
        Debug.Log("Player Respawn");
        yield return new WaitForSeconds(respawnDelay);
        //player.GetComponent<Rigidbody2D>().gravityScale = gravityStore;
        player.transform.position = currentCheckpoint.transform.position;
        player.enabled = true;
        player.GetComponent<Renderer>().enabled = true;
        healthmanager.FullHealth();
        healthmanager.isDead = false;
        Camera.isFollowing = true;
        Instantiate(respawnParticle, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
    }


}
 