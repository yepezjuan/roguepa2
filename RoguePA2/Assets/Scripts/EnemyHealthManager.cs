﻿using UnityEngine;
using System.Collections;

public class EnemyHealthManager : MonoBehaviour {

    public int enemyHealth;
    public int pointsPerDeath;

    public GameObject deathAffect;

	
	void Start () {
	
	}
	
	
	void Update () {

        if (enemyHealth < 0)
        {
            Instantiate(deathAffect, transform.position, transform.rotation);
            ScoreManager.AddPoints(pointsPerDeath);
            Destroy(gameObject);
        }
	
	}

    public void giveDamage(int damageToGive)
    {
        enemyHealth -= damageToGive;
        GetComponent<AudioSource>().Play();
    }
    

}
