﻿using UnityEngine;
using System.Collections;

public class EnemyPatrol : MonoBehaviour {

    public float moveSpeed;
    public bool moveRight;

    public Transform WallCheck;
    public LayerMask whatisWall;
    private bool hittingWall;
    public float WallcheckRadius;

    private bool NotAtEdge;
    public Transform EdgeCheck;



    void Start () {
	
	}
	
	
	void Update () {

        hittingWall = Physics2D.OverlapCircle(WallCheck.position, WallcheckRadius, whatisWall);
        NotAtEdge = Physics2D.OverlapCircle(EdgeCheck.position, WallcheckRadius, whatisWall);

        if (hittingWall || !NotAtEdge)
        {
            moveRight = !moveRight;
        }


        if (moveRight)
        {
            transform.localScale = new Vector3(-1f,1f,1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }

        else
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
           
	
	}
}
